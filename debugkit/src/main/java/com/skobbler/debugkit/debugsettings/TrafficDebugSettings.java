package com.skobbler.debugkit.debugsettings;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.skobbler.debugkit.R;
import com.skobbler.debugkit.util.DebugKitUtils;
import com.skobbler.ngx.SKCoordinate;
import com.skobbler.ngx.map.SKAnimationSettings;
import com.skobbler.ngx.map.SKAnnotation;
import com.skobbler.ngx.map.SKCoordinateRegion;
import com.skobbler.ngx.map.SKMapCustomPOI;
import com.skobbler.ngx.map.SKMapPOI;
import com.skobbler.ngx.map.SKMapSettings;
import com.skobbler.ngx.map.SKMapSurfaceListener;
import com.skobbler.ngx.map.SKMapViewHolder;
import com.skobbler.ngx.map.SKPOICluster;
import com.skobbler.ngx.map.SKScreenPoint;
import com.skobbler.ngx.map.traffic.SKTrafficIncident;
import com.skobbler.ngx.navigation.SKNavigationManager;
import com.skobbler.ngx.reversegeocode.SKReverseGeocoderManager;
import com.skobbler.ngx.routing.SKRouteAlternativeSettings;
import com.skobbler.ngx.routing.SKRouteInfo;
import com.skobbler.ngx.routing.SKRouteJsonAnswer;
import com.skobbler.ngx.routing.SKRouteListener;
import com.skobbler.ngx.routing.SKRouteManager;
import com.skobbler.ngx.routing.SKRouteSettings;
import com.skobbler.ngx.search.SKSearchResult;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AlexandraP on 31.07.2015.
 */
public class TrafficDebugSettings extends DebugSettings implements SKMapSurfaceListener, SKRouteListener {

    private static final byte GREEN_PIN_ICON_ID = 0;

    private static final byte DESTINATION_PIN_ICON_ID = 1;

    private boolean isStartLocationPressed = false, isDestLocationPressed = false;

    private Context context;
    /**
     * The start point(long/lat) for the route.
     */
    private SKCoordinate startPoint = new SKCoordinate(13.73954, 52.83717);
    ;

    /**
     * The destination(long/lat) point for the route
     */
    private SKCoordinate destinationPoint = new SKCoordinate(12.55525, 52.41283);
    /**
     * Coordinates for start/destination point
     */
    private EditText startLatitudeText, startLongitudeText, destinationLatText, destinationLongText;
    /**
     * Traffic mode
     */
    private SKMapSettings.SKTrafficMode trafficMode = SKMapSettings.SKTrafficMode.FLOW_AND_INCIDENTS;
    /**
     * Indicates whether to use live traffic data for calculating best possible
     * route. For this the live traffic has to be enabled first.
     */
    private boolean useLiveTraffic = true;

    private boolean useLiveTrafficR1 = true, useLiveTrafficR2 = true, useLiveTrafficR3 = true;
    /**
     * traffic delay for a specific route
     */
    public static String trafficDelayFirstAlternativeRoute;

    public static String trafficDelaySecondAlternativeRoute;

    public static String trafficDelayThirdAlternativeRoute;
    /**
     * traffic incidents list for alternative route
     */
    public static List<SKTrafficIncident> trafficIncidentsListFirstAlternativeRoute;

    public static List<SKTrafficIncident> trafficIncidentsListSecondAlternativeRoute;

    public static List<SKTrafficIncident> trafficIncidentsListThirdAlternativeRoute;
    /**
     * Contains information about a computed route. Key is : the route mode
     */
    private List<SKRouteInfo> skRouteInfoList = new ArrayList<>();
    /**
     * incidentId the id of the incident
     */
    private int incidentId = 1;
    /**
     * Custom traffic refresh rate in seconds
     */
    private int seconds = 1;
    private float zoomLevel = 8.97f;

    private NumberFormat numberFormat = new DecimalFormat("##.#####");


    @Override
    List<Pair<String, Object>> defineKeyValuePairs() {
        List<Pair<String, Object>> keyValuePairs = new ArrayList<Pair<String, Object>>();
        context = specificLayout.getContext();

        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.traffic_settings), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.start_lat_traffic), numberFormat.format(startPoint.getLatitude())));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.start_long_traffic), numberFormat.format(startPoint.getLongitude())));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.select_start_coord), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.dest_lat_traffic), numberFormat.format(destinationPoint.getLatitude())));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.dest_long_traffic), numberFormat.format(destinationPoint.getLongitude())));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.select_dest_coord), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.traffic_mode), trafficMode.toString()));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.route_1_traffic), useLiveTrafficR1));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.route_2_traffic), useLiveTrafficR2));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.route_3_traffic), useLiveTrafficR3));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.refresh_interval_traffic), seconds));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.use_live_traffic), useLiveTraffic));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.cluster_incidents), false));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.render_route_only_incidents), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.actions_title_traffic), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.calculate_route_traffic_testing), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.clear_all_routes), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.incident_id), incidentId));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.block_incident_id), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.unblock_incident_id), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.info_traffic), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.route_1), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.route_2), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.route_3), null));


        return keyValuePairs;
    }

    @Override
    int defineSpecificLayout() {
        return R.layout.traffic_debug_settings;
    }

    @Override
    void defineSpecificListeners() {
        startLatitudeText = (EditText) specificLayout.findViewById(R.id.start_lat_traffic).findViewById(R.id.property_value);
        startLongitudeText = (EditText) specificLayout.findViewById(R.id.start_long_traffic).findViewById(R.id.property_value);
        destinationLatText = (EditText) specificLayout.findViewById(R.id.dest_lat_traffic).findViewById(R.id.property_value);
        destinationLongText = (EditText) specificLayout.findViewById(R.id.dest_long_traffic).findViewById(R.id.property_value);

        specificLayout.findViewById(R.id.cluster_incidents).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) specificLayout.findViewById(R.id.cluster_incidents).findViewById(R.id.property_value);
                checkBox.setChecked(!checkBox.isChecked());
                activity.getMapView().setTrafficIncidentsClustering(checkBox.isChecked());
            }
        });
        specificLayout.findViewById(R.id.render_route_only_incidents).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) specificLayout.findViewById(R.id.render_route_only_incidents).findViewById(R.id.property_value);
                checkBox.setChecked(!checkBox.isChecked());
                if (checkBox.isChecked()) {
                    activity.getMapView().getMapSettings().setTrafficMode(SKMapSettings.SKTrafficMode.INCIDENTS_ONLY);
                } else {
                    activity.getMapView().getMapSettings().setTrafficMode(trafficMode);
                }
            }
        });
        specificLayout.findViewById(R.id.use_live_traffic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) specificLayout.findViewById(R.id.use_live_traffic).findViewById(R.id.property_value);
                checkBox.setChecked(!checkBox.isChecked());
                useLiveTraffic = checkBox.isChecked();
            }
        });
        specificLayout.findViewById(R.id.route_1_traffic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) specificLayout.findViewById(R.id.route_1_traffic).findViewById(R.id.property_value);
                checkBox.setChecked(!checkBox.isChecked());
                useLiveTrafficR1 = checkBox.isChecked();
            }
        });
        specificLayout.findViewById(R.id.route_2_traffic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) specificLayout.findViewById(R.id.route_2_traffic).findViewById(R.id.property_value);
                checkBox.setChecked(!checkBox.isChecked());
                useLiveTrafficR2 = checkBox.isChecked();
            }
        });
        specificLayout.findViewById(R.id.route_3_traffic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) specificLayout.findViewById(R.id.route_3_traffic).findViewById(R.id.property_value);
                checkBox.setChecked(!checkBox.isChecked());
                useLiveTrafficR3 = checkBox.isChecked();
            }
        });

        SeekBar seekBarRefreshInterval = (SeekBar) specificLayout.findViewById(R.id.refresh_interval).findViewById(R.id.property_seekbar);
        seekBarRefreshInterval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean b) {
                ((TextView) specificLayout.findViewById(R.id.refresh_interval).findViewById(R.id.property_value)).setText(value + "");
                seconds = value;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                activity.getMapView().useCustomTraffic(true);
                activity.getMapView().setRefreshRateForCustomTraffic(seconds);
            }
        });

        specificLayout.findViewById(R.id.select_start_traffic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isStartLocationPressed = true;
                isDestLocationPressed = false;
                Toast.makeText(context, context.getResources().getString(R.string.long_tap_start_location),
                        Toast.LENGTH_LONG).show();
            }
        });
        specificLayout.findViewById(R.id.select_dest_traffic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isStartLocationPressed = false;
                isDestLocationPressed = true;
                Toast.makeText(context, context.getResources().getString(R.string.long_tap_destination_location),
                        Toast.LENGTH_LONG).show();
            }
        });
        final EditText incidentIdValue = (EditText) specificLayout.findViewById(R.id.indicent_id).findViewById(R.id.property_value);
        incidentIdValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String incidentValue = incidentIdValue.getText().toString();
                if (!incidentValue.equals("")) {
                    incidentId = Integer.parseInt(incidentValue);
                }
            }
        });


        specificLayout.findViewById(R.id.block_incident_with_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SKNavigationManager.getInstance().blockIncident(incidentId);
            }
        });
        specificLayout.findViewById(R.id.unblock_incident_with_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SKNavigationManager.getInstance().unblockIncident(incidentId);
            }
        });
        specificLayout.findViewById(R.id.traffic_mode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DebugSettings.getInstanceForType(TrafficModeDebugSettings.class).open(debugBaseLayout, TrafficDebugSettings.this);
            }
        });
        specificLayout.findViewById(R.id.calculate_routes_traffic_debug_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchAlternativeRouteCalculationWithTraffic();
            }
        });
        specificLayout.findViewById(R.id.route1_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DebugSettings.getInstanceForType(TrafficFirstRouteInfo.class).open(debugBaseLayout, TrafficDebugSettings.this);
            }
        });
        specificLayout.findViewById(R.id.route2_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DebugSettings.getInstanceForType(TrafficSecondRouteInfo.class).open(debugBaseLayout, TrafficDebugSettings.this);
            }
        });
        specificLayout.findViewById(R.id.route3_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DebugSettings.getInstanceForType(TrafficThirdRouteInfo.class).open(debugBaseLayout, TrafficDebugSettings.this);
            }
        });
        specificLayout.findViewById(R.id.clear_all_routes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // clear the alternative routes
                SKRouteManager.getInstance().clearRouteAlternatives();
                // clear the selected route
                SKRouteManager.getInstance().clearCurrentRoute();
                // clear list of skRouteInfo
                skRouteInfoList.clear();
            }
        });

    }

    @Override
    void applyCustomChangesToUI() {
        super.applyCustomChangesToUI();
        ((SeekBar) specificLayout.findViewById(R.id.refresh_interval).findViewById(R.id.property_seekbar)).setMax(50);
    }

    /**
     * Launches the calculation of three alternative routes with traffic
     */
    private void launchAlternativeRouteCalculationWithTraffic() {
        skRouteInfoList.clear();
        List<SKRouteAlternativeSettings> routeAlternativeSettingsList = new ArrayList<SKRouteAlternativeSettings>();
        initializeTrafficInformation();
        if (!isStartLocationPressed && !isDestLocationPressed) {
            SKAnnotation annotationStart = new SKAnnotation(GREEN_PIN_ICON_ID);
            annotationStart.setUniqueID(GREEN_PIN_ICON_ID);
            annotationStart.setAnnotationType(SKAnnotation.SK_ANNOTATION_TYPE_GREEN);
            annotationStart.setLocation(startPoint);
            annotationStart.setMininumZoomLevel(5);
            activity.getMapView().addAnnotation(annotationStart,
                    SKAnimationSettings.ANIMATION_NONE);
            SKAnnotation annotationDest = new SKAnnotation(DESTINATION_PIN_ICON_ID);
            annotationDest.setUniqueID(DESTINATION_PIN_ICON_ID);
            annotationDest.setAnnotationType(SKAnnotation.SK_ANNOTATION_TYPE_DESTINATION_FLAG);
            annotationDest.setLocation(destinationPoint);
            annotationDest.setMininumZoomLevel(5);
            activity.getMapView().addAnnotation(annotationDest,
                    SKAnimationSettings.ANIMATION_NONE);
        }
        SKRouteSettings route = new SKRouteSettings();
        route.setStartCoordinate(startPoint);
        route.setDestinationCoordinate(destinationPoint);
        //route.setRouteMode(SKRouteSettings.SKRouteMode.CAR_FASTEST);
        route.setNoOfRoutes(3);
        route.setUseLiveTraffic(useLiveTraffic);
        route.setUseLiveTrafficETA(true);

        //route 1
        SKRouteAlternativeSettings routeTraffic1 = new SKRouteAlternativeSettings(SKRouteSettings.SKRouteMode.CAR_SHORTEST, useLiveTrafficR1, useLiveTrafficR1);
        routeAlternativeSettingsList.add(routeTraffic1);
        //route 2
        SKRouteAlternativeSettings routeTraffic2 = new SKRouteAlternativeSettings(SKRouteSettings.SKRouteMode.CAR_SHORTEST, useLiveTrafficR2, useLiveTrafficR2);
        routeAlternativeSettingsList.add(routeTraffic2);
        //route 3
        SKRouteAlternativeSettings routeTraffic3 = new SKRouteAlternativeSettings(SKRouteSettings.SKRouteMode.CAR_SHORTEST, useLiveTrafficR3, useLiveTrafficR3);
        routeAlternativeSettingsList.add(routeTraffic3);
        route.setAlternativeRouteModes(routeAlternativeSettingsList);
        SKRouteManager.getInstance().setTrafficRoutingMode(trafficMode);
        SKRouteManager.getInstance().setRouteListener(this);
        SKRouteManager.getInstance().calculateRoute(route);

    }

    @Override
    public void onActionPan() {

    }

    @Override
    public void onActionZoom() {

    }

    @Override
    public void onSurfaceCreated(SKMapViewHolder skMapViewHolder) {

    }

    @Override
    public void onMapRegionChanged(SKCoordinateRegion skCoordinateRegion) {

    }

    @Override
    public void onMapRegionChangeStarted(SKCoordinateRegion skCoordinateRegion) {

    }

    @Override
    public void onMapRegionChangeEnded(SKCoordinateRegion skCoordinateRegion) {

    }

    @Override
    public void onDoubleTap(SKScreenPoint skScreenPoint) {

    }

    @Override
    public void onSingleTap(SKScreenPoint skScreenPoint) {

    }

    @Override
    public void onRotateMap() {

    }

    @Override
    public void onLongPress(SKScreenPoint skScreenPoint) {
        SKCoordinate poiCoordinates = activity.getMapView().pointToCoordinate(skScreenPoint);
        final SKSearchResult place = SKReverseGeocoderManager
                .getInstance().reverseGeocodePosition(poiCoordinates);


        boolean selectPoint = isStartLocationPressed || isDestLocationPressed;
        if (poiCoordinates != null && place != null && selectPoint) {
            SKAnnotation annotation = new SKAnnotation(GREEN_PIN_ICON_ID);
            if (isStartLocationPressed) {
                annotation.setUniqueID(GREEN_PIN_ICON_ID);
                annotation
                        .setAnnotationType(SKAnnotation.SK_ANNOTATION_TYPE_GREEN);
                startPoint = place.getLocation();
                startLongitudeText.setText(numberFormat.format(startPoint.getLongitude()));
                startLatitudeText.setText(numberFormat.format(startPoint.getLatitude()));
            } else if (isDestLocationPressed) {
                annotation.setUniqueID(DESTINATION_PIN_ICON_ID);
                annotation
                        .setAnnotationType(SKAnnotation.SK_ANNOTATION_TYPE_DESTINATION_FLAG);
                destinationPoint = place.getLocation();
                destinationLongText.setText(numberFormat.format(destinationPoint.getLongitude()));
                destinationLatText.setText(numberFormat.format(destinationPoint.getLatitude()));
            }
            annotation.setLocation(place.getLocation());
            annotation.setMininumZoomLevel(5);
            activity.getMapView().addAnnotation(annotation,
                    SKAnimationSettings.ANIMATION_NONE);
        }
    }

    @Override
    void onOpened() {
        super.onOpened();
        activity.getMapHolder().setMapSurfaceListener(this);
        activity.getMapView().setZoom(zoomLevel);
        activity.getMapView().getMapSettings().setTrafficMode(trafficMode);
        activity.getMapView().setTrafficIncidentsClustering(false);
        activity.getMapView().showTrafficIncidentsFromZoomThreshold(true, 1);

    }

    @Override
    void onClose() {
        super.onClose();
        activity.getMapHolder().setMapSurfaceListener(activity);
    }

    @Override
    void onChildClosed(DebugSettings closedChild) {
        super.onChildClosed(closedChild);
    }

    @Override
    void onChildChanged(DebugSettings changedChild) {
        super.onChildChanged(changedChild);
        if (changedChild instanceof TrafficModeDebugSettings) {
            trafficMode = SKMapSettings.SKTrafficMode.values()[((TrafficModeDebugSettings) changedChild).getCurrentSelectedIndex()];
            ((TextView) specificLayout.findViewById(R.id.traffic_mode).findViewById(R.id.property_value)).setText(trafficMode.toString());
            activity.getMapView().getMapSettings().setTrafficMode(trafficMode);
        }
    }

    @Override
    public void onInternetConnectionNeeded() {

    }

    @Override
    public void onMapActionDown(SKScreenPoint skScreenPoint) {

    }

    @Override
    public void onMapActionUp(SKScreenPoint skScreenPoint) {

    }

    @Override
    public void onPOIClusterSelected(SKPOICluster skpoiCluster) {

    }

    @Override
    public void onMapPOISelected(SKMapPOI skMapPOI) {

    }

    @Override
    public void onAnnotationSelected(SKAnnotation skAnnotation) {

    }

    @Override
    public void onCustomPOISelected(SKMapCustomPOI skMapCustomPOI) {

    }

    @Override
    public void onCompassSelected() {

    }

    @Override
    public void onCurrentPositionSelected() {

    }

    @Override
    public void onObjectSelected(int i) {

    }

    @Override
    public void onTrafficIncidentSelected(SKTrafficIncident skTrafficIncident) {

    }

    @Override
    public void onTrafficIncidentsClusterSelected(List<SKTrafficIncident> list) {

    }

    @Override
    public void onTrafficInfoRefreshed() {

    }

    @Override
    public void onInternationalisationCalled(int i) {

    }

    @Override
    public void onBoundingBoxImageRendered(int i) {

    }

    @Override
    public void onGLInitializationError(String s) {

    }

    @Override
    public void onScreenshotReady(Bitmap bitmap) {

    }

    @Override
    public void onRouteCalculationCompleted(SKRouteInfo skRouteInfo) {
        if (!skRouteInfo.isCorridorDownloaded()) {
            return;
        }
        skRouteInfoList.add(skRouteInfo);
        // zoom to the current route
        SKRouteManager.getInstance().zoomToRoute(1, 1, 8, 8, 8, 8);

    }

    @Override
    public void onRouteCalculationFailed(SKRoutingErrorCode skRoutingErrorCode) {

    }

    @Override
    public void onAllRoutesCompleted() {
        if(useLiveTrafficR1){
            trafficDelayFirstAlternativeRoute = DebugKitUtils.formatTime(skRouteInfoList.get(1).getTrafficDelay());
            trafficIncidentsListFirstAlternativeRoute = skRouteInfoList.get(1).getIncidentsOnRoute();
        }
        if(useLiveTrafficR2){
            trafficDelaySecondAlternativeRoute = DebugKitUtils.formatTime(skRouteInfoList.get(2).getTrafficDelay());
            trafficIncidentsListSecondAlternativeRoute = skRouteInfoList.get(2).getIncidentsOnRoute();
        }
        if(useLiveTrafficR3){
            trafficDelayThirdAlternativeRoute = DebugKitUtils.formatTime(skRouteInfoList.get(3).getTrafficDelay());
            trafficIncidentsListThirdAlternativeRoute = skRouteInfoList.get(3).getIncidentsOnRoute();
        }
        SKRouteManager.getInstance().zoomToRoute(1, 1, 8, 8, 8, 8);
        zoomLevel = activity.getMapView().getZoomLevel();
    }

    private void initializeTrafficInformation(){
        trafficDelayFirstAlternativeRoute = null;
        trafficDelaySecondAlternativeRoute = null;
        trafficDelayThirdAlternativeRoute = null;
        trafficIncidentsListFirstAlternativeRoute = new ArrayList<>();
        trafficIncidentsListSecondAlternativeRoute = new ArrayList<>();
        trafficIncidentsListThirdAlternativeRoute = new ArrayList<>();
    }
    @Override
    public void onServerLikeRouteCalculationCompleted(SKRouteJsonAnswer skRouteJsonAnswer) {

    }

    @Override
    public void onOnlineRouteComputationHanging(int i) {

    }

    public static String getTrafficDelayFirstAlternativeRoute() {
        return trafficDelayFirstAlternativeRoute;
    }

    public static String getTrafficDelaySecondAlternativeRoute() {
        return trafficDelaySecondAlternativeRoute;
    }

    public static String getTrafficDelayThirdAlternativeRoute() {
        return trafficDelayThirdAlternativeRoute;
    }

    public static List<SKTrafficIncident> getTrafficIncidentsListFirstAlternativeRoute() {
        return trafficIncidentsListFirstAlternativeRoute;
    }

    public static List<SKTrafficIncident> getTrafficIncidentsListSecondAlternativeRoute() {
        return trafficIncidentsListSecondAlternativeRoute;
    }

    public static List<SKTrafficIncident> getTrafficIncidentsListThirdAlternativeRoute() {
        return trafficIncidentsListThirdAlternativeRoute;
    }
}
