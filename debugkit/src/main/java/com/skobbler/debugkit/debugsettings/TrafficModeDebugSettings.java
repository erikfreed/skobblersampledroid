package com.skobbler.debugkit.debugsettings;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import android.widget.CheckBox;

import com.skobbler.debugkit.R;
import com.skobbler.ngx.map.SKMapSettings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AlexandraP on 03.08.2015.
 */
public class TrafficModeDebugSettings extends EnumBasedDebugSettings {

    @Override
    Class defineEnumClass() {
        return SKMapSettings.SKTrafficMode.class;
    }

    @Override
    int defineInitialSelectionIndex() {
        return 3;
    }

    @Override
    int defineSpecificLayout() {
        return R.layout.traffic_mode_debug_settings;
    }
}
