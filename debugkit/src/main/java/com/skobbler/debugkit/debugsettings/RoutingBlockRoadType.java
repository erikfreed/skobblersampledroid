package com.skobbler.debugkit.debugsettings;

import android.content.Context;
import android.util.Pair;

import com.skobbler.debugkit.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mirceab on 28.07.2015.
 */
public class RoutingBlockRoadType extends SingleChoiceListDebugSettings {

    @Override
    List<String> defineChoices() {
        List<String> choices = new ArrayList<String>();
        Context context = specificLayout.getContext();
        choices.add(context.getResources().getString(R.string.block_road_mfeet));
        choices.add(context.getResources().getString(R.string.block_road_myards));
        choices.add(context.getResources().getString(R.string.block_road_mmile));
        choices.add(context.getResources().getString(R.string.block_road_mkm));
        return choices;
    }

    @Override
    int defineInitialSelectionIndex() {
        return 0;
    }

    @Override
    int defineSpecificLayout() {
        return R.layout.routing_debug_kit_block_road_type;
    }


}
