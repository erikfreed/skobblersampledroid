package com.skobbler.debugkit.debugsettings;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skobbler.debugkit.R;
import com.skobbler.ngx.map.traffic.SKTrafficIncident;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by AlexandraP on 07.08.2015.
 */
public class TrafficFirstRouteInfo extends DebugSettings {

    /**
     * View for the incident
     */
    ViewGroup incidentItem;

    @Override
    List<Pair<String, Object>> defineKeyValuePairs() {
        List<Pair<String, Object>> keyValuePairs = new ArrayList<Pair<String, Object>>();
        Context context = specificLayout.getContext();

        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.route_1), null));
        return keyValuePairs;
    }

    @Override
    int defineSpecificLayout() {
        return R.layout.traffic_route_info_settings;
    }

    @Override
    void defineSpecificListeners() {

    }

    @Override
    void onOpened() {
        super.onOpened();
        ((TextView) specificLayout.findViewById(R.id.route_delay_info).findViewById(R.id.property_name)).setText("R1: Delay " + TrafficDebugSettings.getTrafficDelayFirstAlternativeRoute());
        LayoutInflater inflater = activity.getLayoutInflater();
        List<SKTrafficIncident> incidents = TrafficDebugSettings.getTrafficIncidentsListFirstAlternativeRoute();

        int position = 1;
        if (incidents != null) {
            for (int i = 0; i < incidents.size(); i++) {
                incidentItem =
                        (ViewGroup) inflater.inflate(R.layout.element_debug_kit_item_textview, null, false);
                String text = "Type: " + incidents.get(i).getType().getValue() + " Delay: " + incidents.get(i).getDelay();
                ((TextView) incidentItem.findViewById(R.id.property_name)).setText(text);
                specificLayout.addView(incidentItem, position);
                position++;
            }
        }
    }

    @Override
    void onClose() {
        super.onClose();
        specificLayout.removeViews(1, specificLayout.getChildCount() - 1);
    }
}

