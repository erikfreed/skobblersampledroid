package com.skobbler.sdkdemo.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.skobbler.ngx.map.traffic.SKTrafficIncident;
import com.skobbler.ngx.routing.SKRouteInfo;
import com.skobbler.ngx.routing.SKRouteManager;
import com.skobbler.sdkdemo.R;
import com.skobbler.sdkdemo.util.DemoUtils;

import java.util.ArrayList;
import java.util.List;


public class TrafficInformationActivity extends Activity {

    private int routeId;

    private TextView totalDuration, length, highWays, tollRoads, ferryLines, incidentsTextView, incidentsDelay, flowDelay, totalDelayText;

    private ListView list, listCluster;

    private View routeInfoView, incidentsClusterView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traffic_information);
        Intent mIntent = getIntent();
        routeId = mIntent.getIntExtra("route_id", 0);
        ArrayList<String> clusterList = (ArrayList<String>) getIntent().getSerializableExtra("cluster_traffic_list");
        routeInfoView = findViewById(R.id.route_info_new_screen);
        incidentsClusterView = findViewById(R.id.incidents_cluster);
        totalDuration = (TextView) findViewById(R.id.total_duration_text_view);
        length = (TextView) findViewById(R.id.lenght_text_view);
        highWays = (TextView) findViewById(R.id.contains_highways);
        tollRoads = (TextView) findViewById(R.id.contains_tollroads);
        ferryLines = (TextView) findViewById(R.id.contains_ferry_lines);
        incidentsDelay = (TextView) findViewById(R.id.incidents_delay);
        flowDelay = (TextView) findViewById(R.id.flow_delay);
        totalDelayText = (TextView) findViewById(R.id.total_delay);
        incidentsTextView = (TextView) findViewById(R.id.incidents);
        list = (ListView) findViewById(R.id.listViewRouteInfo);
        listCluster = (ListView) findViewById(R.id.listViewTraffic);
        if (routeId > 0) {
            routeInfoView.setVisibility(View.VISIBLE);
            setRouteInfo();
        } else if (clusterList.size() > 0) {
            incidentsClusterView.setVisibility(View.VISIBLE);
            ArrayAdapter<String> arrayAdapter =
                    new ArrayAdapter<String>(this, R.layout.layout_search_list_item, R.id.title, clusterList);

            listCluster.setAdapter(arrayAdapter);
        }


    }

    private void setRouteInfo() {
        SKRouteInfo skRouteInfo = SKRouteManager.getInstance().getRouteInfo(routeId);
        List<SKTrafficIncident> incidents = new ArrayList<SKTrafficIncident>();
        ArrayList<String> incidentList = new ArrayList<String>();
        incidents = SKRouteManager.getInstance().getIncidentsOnRouteWithId(routeId, true);
        totalDuration.setText("Total duration:" + DemoUtils.formatTime(skRouteInfo.getEstimatedTime()));
        length.setText("Distance: " + DemoUtils.formatDistance(skRouteInfo.getDistance()));
        totalDelayText.setText("Total delay: " + DemoUtils.formatTime(skRouteInfo.getTrafficDelay()));
        int incidentsTotalDelay = 0;
        int totalDelay = skRouteInfo.getTrafficDelay();
        if (skRouteInfo.isContainsHighWays()) {
            highWays.setText("Contains highways: Yes");
        } else {
            highWays.setText("Contains highways: No");
        }
        if (skRouteInfo.isContainsTollRoads()) {
            tollRoads.setText("Contains toll roads: Yes");
        } else {
            tollRoads.setText("Contains toll roads: No");
        }
        if (skRouteInfo.isContainsFerryLines()) {
            ferryLines.setText("Contains ferry lines: Yes");
        } else {
            ferryLines.setText("Contains ferry lines: No");
        }
        if (incidents.isEmpty()) {
            incidentsTextView.setText("Incidents: No incidents");
        } else {
            incidentsTextView.setText("Incidents: ");
            for (SKTrafficIncident incident : incidents) {
                incidentList.add(incident.getDescription());
                incidentsTotalDelay = incidentsTotalDelay + incident.getDelay();
            }

            ArrayAdapter<String> arrayAdapter =
                    new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, incidentList);

            list.setAdapter(arrayAdapter);
        }
        incidentsDelay.setText("Incidents delay: " + DemoUtils.formatTime(incidentsTotalDelay));
        if (totalDelay > 0) {
            flowDelay.setText("Flow delay: " + DemoUtils.formatTime(totalDelay - incidentsTotalDelay));
            totalDelayText.setText("Total delay: " + DemoUtils.formatTime(totalDelay) );
        } else if (totalDelay < 0) {
            totalDelayText.setText("Total delay: 0s" );
            flowDelay.setText("Flow delay: 0s" );
        } else {
            flowDelay.setText("Flow delay: " + DemoUtils.formatTime(totalDelay));
            totalDelayText.setText("Total delay: " + DemoUtils.formatTime(totalDelay + incidentsTotalDelay));
        }


    }
}
