package com.skobbler.sdkdemo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.skobbler.sdkdemo.R;


public class MapPopUpTrafficActivity extends Activity {
    private String incidentType,incidentRoadName,incidentDesc;
    private TextView textViewType,textViewRoad,textViewDescription;
  
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_pop_up_traffic);
        textViewType = (TextView) findViewById(R.id.textViewType);
        textViewRoad = (TextView) findViewById(R.id.textViewRoadName);
        textViewDescription = (TextView) findViewById(R.id.textViewDesc);
       
        Intent mIntent = getIntent();
        incidentType = mIntent.getExtras().getString("incident_type");
        System.out.println("incidentType:" + incidentType);
        incidentRoadName = mIntent.getExtras().getString("incident_road_name");
        System.out.println("incident road:" + incidentRoadName);
        incidentDesc = mIntent.getExtras().getString("incident_description");
        System.out.println("incident desc:" + incidentDesc);
        
        textViewType.setText("Incident type: " + incidentType);
        textViewRoad.setText("Road: " + incidentRoadName);
        textViewDescription.setText("Description: " + incidentDesc);
     
    }
    
}
